import { named } from 'blackbox-ioc'
import Button from './Button'
import rpio from 'rpio'

// PMOD interface IO:
const SER1       = 7  // GPIO 4
const PMOD_EN1   = 11 // GPIO 17

const SER2       = 29 // GPIO 5
const PMOD_EN2   = 31 // GPIO 6

const SER3       = 37 // GPIO 26
const PMOD_EN3   = 33 // GPIO 13

const SRCLK      = 22 // GPIO 25
const PCLK       = 32 // GPIO 12

// Indicator LED IO:
const LED1       = 12 // GPIO 18

// XVC IO:
const TCK        = 13 // GPIO 27     
const TDO        = 15 // GPIO 22
const TDI        = 16 // GPIO 23
const TMS        = 18 // GPIO 24

const JTAG_SENSE = 35 // GPIO 19

// Button to PMOD bit mappings:
const BUTTON_MAP: {[key: string]: number} = {}
BUTTON_MAP[Button.up.toString()] = 0
BUTTON_MAP[Button.down.toString()] = 1
BUTTON_MAP[Button.left.toString()] = 2
BUTTON_MAP[Button.right.toString()] = 3
BUTTON_MAP[Button.centre.toString()] = 4

@named('basys3GpioService')
export default class Basys3GpioService {
    pmod1Value = 0
    pmod2Value = 0
    pmod3Value = 0

    constructor() {
        // rpio.init({

        // });

        [
            { pin: SER1,     dir: rpio.OUTPUT, def: rpio.LOW},
            { pin: PMOD_EN1, dir: rpio.OUTPUT, def: rpio.LOW},
            { pin: SER2,     dir: rpio.OUTPUT, def: rpio.LOW},
            { pin: PMOD_EN2, dir: rpio.OUTPUT, def: rpio.LOW},
            { pin: SER3,     dir: rpio.OUTPUT, def: rpio.LOW},
            { pin: PMOD_EN3, dir: rpio.OUTPUT, def: rpio.LOW},
            { pin: SRCLK,    dir: rpio.OUTPUT, def: rpio.LOW},
            { pin: PCLK,     dir: rpio.OUTPUT, def: rpio.LOW},
        ].forEach(({pin, dir, def}) => rpio.open(pin, dir, def))
    }

    /**
     * @param clear A bitmask for the 16 Basys3 switches.
     */
    setSwitchState(set: number) {
        this.pmod1Value = set&0xff // 8 least significant bits go into PMOD1
        this.pmod2Value = (set>>8)&0xff // 8 most significant bits go into PMOD2
    }

    private makeMask(buttons: Button[]) {
        return buttons.reduce(
            (mask: number, button: Button) => mask | 1 << BUTTON_MAP[button.toString()], 
            0
        )
    }

    setButtonState(pressed: Button[], released: Button[]) {
        this.pmod3Value |= this.makeMask(pressed) // set pressed bits
        this.pmod3Value &= ~this.makeMask(released) // clear released bits
    }

    /**
     * Writes out the PMOD values, MSB first.
     */
    update() {
        rpio.write(LED1, rpio.HIGH)
        rpio.write(PCLK, rpio.LOW)
        for(let i = 7; i >= 0; --i) {
            rpio.write(SRCLK, rpio.LOW)
            rpio.write(SER1, (this.pmod1Value >> i) & 0x1 ? rpio.HIGH : rpio.LOW)
            rpio.write(SER2, (this.pmod2Value >> i) & 0x1 ? rpio.HIGH : rpio.LOW)
            rpio.write(SER3, (this.pmod3Value >> i) & 0x1 ? rpio.HIGH : rpio.LOW)
            rpio.sleep(0.001) // 1kHz update
            rpio.write(SRCLK, rpio.HIGH) // push serial data through with a rising edge
        }
        rpio.write(PCLK, rpio.HIGH) // push parallel data through with a rising edge
        rpio.write(LED1, rpio.LOW)
    }
}