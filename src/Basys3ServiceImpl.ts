import {serviceClass, autowired} from 'blackbox-ioc'
import {makeServiceObject} from 'blackbox-services'
import Basys3 from './Basys3'
import {Service} from 'blackbox-services'
import {NamedReference} from 'blackbox-services'
import Button from './Button'
import Basys3GpioService from 'Basys3GpioService'
import fs from 'fs'

@serviceClass('basys3-service')
export class Basys3ServiceImpl {

  @autowired('oasDoc')
  oasDoc:any

  @autowired('basys3GpioService')
  basys3GpioService!: Basys3GpioService

  board: Basys3 = {
    switches: {
      set: 0,
      clear: 0xff
    },
    buttons: {
      pressed: [],
      released: [Button.up, Button.down, Button.left, Button.right, Button.centre]
    }
  }

  constructor() {
  }
  
  getBasys3List(props:any):Basys3[] {
    return [this.board]
  }

  getOptionsForBasys3Service(props:any):Service {
    return makeServiceObject(this.oasDoc, 'basys3')
  }

  getBasys3(props:any):Basys3 {
    return this.board
  }

  getOptionsForBasys3(props:any):Service {
    return makeServiceObject(this.oasDoc, 'basys3')
  }

  private union(a: any[], b: any[]) {
    return a.concat(b.filter(item => a.indexOf(item) < 0))
  }

  updateBasys3({basys3, req}:{basys3: Basys3, req: any}):NamedReference {
    const path = '/opt/blackbox/state.json'
    if(fs.existsSync(path)) {
      fs.promises.readFile(path)
      .then(state => JSON.parse(state.toString()))
      .then((state: any) => {
        const auth = req.headers.authorization.split(' ')
        const bearer = auth[0]?.toLowerCase() === 'bearer' ? auth[1] : ''
        if(state.owner && bearer !== state.owner?.username) {
          console.log(`Unauthorised access: Owner is ${state.owner?.username}, but request is from ${bearer}.`);
          return
        }

        if(typeof basys3.switches?.set === 'number') {
          this.board.switches!.set |= basys3.switches.set // merge in the new set bits
          this.board.switches!.clear &= ~basys3.switches.set // zero the corresponding clear bits
        }
        if(typeof basys3.switches?.clear === 'number') {
          this.board.switches!.clear |= basys3.switches.clear // merge in the new clear bits
          this.board.switches!.set &= ~basys3.switches.clear // zero the corresponding set bits
        }
        if(Array.isArray(basys3.buttons?.pressed)) {
          this.board.buttons!.pressed = this.union(<Button[]>this.board.buttons!.pressed, basys3.buttons!.pressed) // merge without duplicates
          this.board.buttons!.released = this.board.buttons!.released!.filter(item => this.board.buttons!.pressed!.indexOf(item) < 0) // remove newly pressed buttons from the released list
        }
        if(Array.isArray(basys3.buttons?.released)) {
          this.board.buttons!.released = this.union(<Button[]>this.board.buttons!.released, basys3.buttons!.released) // merge without duplicates
          this.board.buttons!.pressed = this.board.buttons!.pressed!.filter(item => this.board.buttons!.released!.indexOf(item) < 0) // remove newly released buttons from the pressed list
        }

        // Set the GPIO outputs:
        this.basys3GpioService.setSwitchState(this.board.switches!.set)
        this.basys3GpioService.setButtonState(this.board.buttons!.pressed, this.board.buttons!.released)
        this.basys3GpioService.update()
      })
      .catch(console.error)
    } else {
      console.log(`${path} not found; user ${req.headers.authorization} is therefore unauthorised`);
    }

    return {name: 'basys3'}
  }
}
