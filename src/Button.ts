enum Button {
  up = "up",
  down = "down",
  left = "left",
  right = "right",
  centre = "centre"
}
export default Button
