import Switches from './Switches'
import Buttons from './Buttons'

export default interface Basys3 {
  switches?: Switches
  buttons?: Buttons
}

