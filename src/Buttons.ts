import Button from './Button'

export default interface Buttons {
  pressed: Button[]
  released: Button[]
}

