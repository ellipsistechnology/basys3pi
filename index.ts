/*
 * Blackbox API for 
 *
 */

'use strict';

import {factory} from 'blackbox-ioc'
import {DefaultRuleBase} from 'blackbox-rules-utils'
import RuleBase from 'blackbox-rules'
import cors from 'cors'
import {loadConfig} from 'blackbox-config'

var fs = require('fs'),
    path = require('path'),
    http = require('http'),
    https = require('https'),
    glob = require('glob'),
    path = require('path'),
    app = require('connect')(),
    oas3Tools = require('oas3-tools'),
    jsyaml = require('js-yaml'),
    serverPort = 8090;

// Load all files to ensure annotations are read:
glob
  .sync(`${__dirname}/*src/**/*.js`)
  .forEach( (file:string) => {
    require(path.resolve(file))
  } )

// Load blackbox configuration:
let config: any
try {
  config = loadConfig()
  
} catch(error) {
  console.error(error)
}

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync(path.join(__dirname,'api/openapi.yaml'), 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);

class _InitIoc {
  @factory('rulebase')
  createRuleBase():RuleBase {
    return new DefaultRuleBase()
  }

  @factory('oasDoc')
  getOasDoc() {
    return swaggerDoc
  }
}

import initRulesService from 'blackbox-rules-service'
initRulesService()

import initRootServices from 'blackbox-root-service'
import Basys3Service from 'Basys3GpioService';
initRootServices()

// swaggerRouter configuration
var options = {
  swaggerUi: path.join(__dirname, '/openapi.json'),
  controllers: path.join(__dirname, './gensrc/controllers'),
  useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
};

// Initialize the Swagger middleware
oas3Tools.initializeMiddleware(swaggerDoc, function (middleware:any) {

  app.use(cors());

  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));

  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());

  // Start the server
  // http.createServer(app).listen(serverPort, function () {
  //   console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
  //   console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
  // });
  ( config.agent.tls ?
    https.createServer({
      key:  fs.readFileSync(config.agent.tls.key),
      cert: fs.readFileSync(config.agent.tls.cert),
      ca:   config.agent.tls.ca ? fs.readFileSync(config.agent.tls.ca) : undefined,
    }, app) :
    http.createServer(app)
  ).listen(serverPort, function () {
    console.log(`Your server is listening on port ${serverPort} (${config.agent.tls ? 'https' : 'http'}://localhost:${serverPort})`);
    console.log(`Swagger-ui is available on ${config.agent.tls ? 'https' : 'http'}://localhost:${serverPort}/docs`);
  });

});
